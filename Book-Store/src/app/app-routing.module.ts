import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddingbookComponent } from './addingbook/addingbook.component';
import { ListingBookComponent } from './listing-book/listing-book.component';
import { ListBookComponent } from './list-book/list-book.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { UpdateBookComponent } from './update-book/update-book.component';

const routes: Routes = [
  {path: '', redirectTo: 'listbooks', pathMatch: 'full'},
  {path: 'addbooks', component: AddingbookComponent },
  {path: 'listbooks', component: ListBookComponent},
  {path: 'book-details/:Slno', component: BookDetailsComponent },
  {path: 'update-book/:Slno', component: UpdateBookComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

