import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddingbookComponent } from './addingbook/addingbook.component';
import { ListingBookComponent } from './listing-book/listing-book.component';
import { FormsModule } from '@angular/forms';
import { ListBookComponent } from './list-book/list-book.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { UpdateBookComponent } from './update-book/update-book.component';

@NgModule({
  declarations: [
    AppComponent,
    AddingbookComponent,
    ListingBookComponent,
    ListBookComponent,
    BookDetailsComponent,
    UpdateBookComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
