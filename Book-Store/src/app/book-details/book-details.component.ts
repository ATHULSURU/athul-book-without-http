import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { BookService } from '../book.service';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {

  public book: User[];
  public index;
  public oneBook;
  constructor(private bookService: BookService, private route: ActivatedRoute ) {
    this.book = this.bookService.getBooks();
   }

    ngOnInit() {
      this.route.paramMap.subscribe((params: ParamMap) => {
      this.index = parseInt(params.get('Slno'), 10);
      this.oneBook = this.book[this.index];
    });

  }

}
