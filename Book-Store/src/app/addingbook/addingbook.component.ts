import { Component, OnInit } from '@angular/core';
import { BookService } from '../book.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addingbook',
  templateUrl: './addingbook.component.html',
  styleUrls: ['./addingbook.component.css']
})
export class AddingbookComponent {

  constructor(private bookService: BookService, private router: Router) { }
  public bookAdded = false;

  onClickSubmit(data) {
    this.bookService.addingbook(data);
    this.router.navigate(['/listbooks']);
    this.bookAdded = true;
  }

}
