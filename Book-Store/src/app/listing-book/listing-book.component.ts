import { Component, OnInit, Input } from '@angular/core';
import { BookService } from '../book.service';
import { Router } from '@angular/router';
import { User } from '../user';

@Component({
  selector: 'app-listing-book',
  templateUrl: './listing-book.component.html',
  styleUrls: ['./listing-book.component.css']
})
export class ListingBookComponent {
  @Input() public parentData: User[];
  @Input() public i: number;
  constructor(private bookService: BookService, private router: Router) { }
  onSelect() {
    this.router.navigate(['/book-details', this.i]);
  }
  toDelete(i) {
    this.bookService.deleteBook(i);
  }
  toUpdate() {
    this.router.navigate(['/update-book', this.i]);
  }


}
